package arRelay_java;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.opencsv.CSVReader;

/*
 * Description:
 * ------------
 * arrelayJava triggers arduino relays using predefine time. 
 * time variables are store in Res/configuration file and are read each day at 0:00.
 * Currently there're 4 relays connected to Arduino board so 4 thread are initiated for each relay
 * Each thread is assign with day start time and duration of relay on operation and sleeps until that time arrive
 * Operation start with http ON command sends to relay board 10.0.0.{ARDUINO_ROUTER_PORT}
 * During operation Thread sleep for duration, Operation stops with OFF command  
 * 
 * 1. Main flow:
 * 1.1 Read configuration file
 * 1.2 Start Threads
 * 1.3 Forever
 * 1.3.1 Wait until {configurationReadTime} 
 * 1.3.2 Read configuration
 * 1.3.3 Update global variables
 * 
 * 2. Parameters:
 * 2.1 NUMBER_OF_RELAYS
 * 2.2 CFG_FILE
 * 
 * 3. Variables:
 * 3.1 arduinoRelayStatus
 * 3.2 configurationReadTime
 * 3.3 relayRequest
 * 3.4 currentTime
 * 
 * 
 * 
 */
public class ArRelayJava {

	private static final String TAG = "[Arduino relay Control] ";
	
	// ------------------------------- //
	// -- 2. Parameters             -- //
	// ------------------------------- //
	
	// 2.1 NUMBER_OF_RELAYS
	private static final int NUMBER_OF_RELAYS = 4;
	
	// 2.2 CFG_FILE
	private static final String CFG_FILE = "/home/idanhahn/workarea/git/arRelayJava/arRelay_java/exec/Res/configuration.cfg"; 
	
	// ------------------------------- //
	// -- 3. Variables              -- //
	// ------------------------------- //
	
	// 3.1 arduino relay status
	public static char[] arduinoRelayStatus = new char[NUMBER_OF_RELAYS]; 

	// 3.2 configuration read time
	public static String configurationReadTime = null;
	
	// 3.3 relay request
	public static RelayReqType relayReq[] = new RelayReqType[NUMBER_OF_RELAYS];
	
	public static void main(String[] args) {
		
		// ------------------------------- //
		// -- 1. Main flow				-- //
		// ------------------------------- //
		
		// 1.1 Read configuration file
		readCfgFile();
		
		// 1.2 Start Threads
		for (int i = 0; i < NUMBER_OF_RELAYS; i++){
			
			SingleRelayThread relayThread = new SingleRelayThread(i,relayReq[i]);
			relayThread.run();
			
		}
		// 1.3 Forever
		while (true){
			// 1.3.1 Wait until {configurationReadTime}
			String[] nextCfgReadTimeStr = configurationReadTime.split(":"); 
			int startHour 	= Integer.parseInt(nextCfgReadTimeStr[0]);
			int startMinute	= Integer.parseInt(nextCfgReadTimeStr[1]);
			System.out.println(TAG + "next configuration read time {Hour,Minute} " + startHour + ", " + startMinute);		
			System.out.println(relayReq);
			// 1.2 Wait for start operation time
		
		
			Calendar currentTime = Calendar.getInstance();
			Calendar startTime = GregorianCalendar.getInstance();
			startTime.set(Calendar.HOUR_OF_DAY, startHour);
			startTime.set(Calendar.MINUTE, startMinute);
			startTime.add(Calendar.DAY_OF_WEEK, 1);
			try {
				Thread.sleep(startTime.getTimeInMillis() - currentTime.getTimeInMillis());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// 1.3.2 Read configuration
			readCfgFile();
			// 1.3.3 Update global variables
			
			for (int i = 0; i < NUMBER_OF_RELAYS; i++){
				
				SingleRelayThread relayThread = new SingleRelayThread(i,relayReq[i]);
				relayThread.run();
				
			}
		}
	}

	public static void readCfgFile(){
		
		try {
			
			CSVReader csvReader = new CSVReader(new FileReader(CFG_FILE));
			String[] nextLine;
			
			while ((nextLine = csvReader.readNext()) != null){
				if (nextLine[0].equals("cfg")){

					configurationReadTime = nextLine[1];
					System.out.println(TAG + "read configuration time: " + configurationReadTime);
					
				} else if (nextLine[0].equals("relay")){

					int relayNumber = Integer.parseInt(nextLine[1]);
					RelayReqType nextRelayReq = new RelayReqType(nextLine[2],Integer.parseInt(nextLine[3]),nextLine[4]);
					relayReq[relayNumber] = nextRelayReq;
					
				} else {
					System.out.println(TAG + nextLine[0]);
					System.err.println(TAG + "Configuration file read has failed, line doesn't contain relay or cfg option");

				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public String printTime(){
		Calendar currentTime = new GregorianCalendar();
		return currentTime.toString();
	}

}
