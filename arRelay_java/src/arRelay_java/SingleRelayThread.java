package arRelay_java;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.GregorianCalendar;

/*
 * Description:
 * ------------
 * 
 * 
 * 1. Thread flow:
 * 1.1 parse relay request
 * 1.2 Wait for start operation time
 * 1.3 Lock arduinoRelayStatus
 * 1.4 Send ON 
 * 1.5 Release lock
 * 1.6 Wait duration
 * 1.7 Lock arduinoRelayStatus
 * 1.8 Send OFF
 * 1.9 Release lock
 * 
 * 2. Parameters:
 * 2.1 NUMBER_OF_RELAYS
 * 2.2 CFG_FILE
 * 
 * 3. Variables:
 * 3.1 arduino status
 * 3.2 relay number
 * 3.3 relay request
 * 
 * 
 * 
 */

public class SingleRelayThread extends Thread{
	
	// ------------------------------- //
	// -- 2. Parameters             -- //
	// ------------------------------- //
	
	// 2.1 NUMBER_OF_RELAYS
	private static final int NUMBER_OF_RELAYS = 4;	
	
	// ------------------------------- //
	// -- 3. Variables              -- //
	// ------------------------------- //
	
	// 3.1 arduino status
	static char[] arduinoRelayStatus = new char[NUMBER_OF_RELAYS];
	private static final Object lock = new Object();
	
	// 3.2 relay number
	int relayNum;
	
	// 3.3 relay request
	RelayReqType relayReq;

	public SingleRelayThread(int relayNum,RelayReqType relayReq){
		this.relayNum = relayNum;
		this.relayReq = relayReq;
		for (int i = 0; i < NUMBER_OF_RELAYS; i++){
			arduinoRelayStatus[i] = '0';
		}
		System.out.println(new String(arduinoRelayStatus));
	}
	
	
	public void run(){
	
		// ------------------------------- //
		// -- 1. Thread flow            -- //
		// ------------------------------- //
			
		// 1.1 parse relay request
		if (relayReq == null){
			System.err.println("Error in relay " + relayNum + " no relay request");
			return;
		}
		
		if (relayReq.onDays.equals("none")){
			return;
		}
		
		String[] startTimeStr = relayReq.startTime.split(":"); 
		int startHour 	= Integer.parseInt(startTimeStr[0]);
		int startMinute	= Integer.parseInt(startTimeStr[1]);
		System.out.println("Start time on thread number " +  relayNum + "{Hour,Minute} " + startHour + ", " + startMinute);		
		System.out.println(relayReq);
		// 1.2 Wait for start operation time
		
		
		Calendar currentTime = Calendar.getInstance();
		Calendar startTime = GregorianCalendar.getInstance();
		startTime.set(Calendar.HOUR_OF_DAY, startHour);
		startTime.set(Calendar.MINUTE, startMinute);
		
		try {
			sleep(startTime.getTimeInMillis() - currentTime.getTimeInMillis());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		// 1.3 Lock arduinoRelayStatus
		synchronized(lock){
			
			// 1.4 Send ON 
			arduinoRelayStatus[relayNum] = '1';
			String arduinoReq = new String("http://idanhahn.info.tm:82/" + new String(arduinoRelayStatus));
			System.out.println(arduinoReq);
			try {
				URL arduinoUrl = new URL(arduinoReq);
				HttpURLConnection con = (HttpURLConnection) arduinoUrl.openConnection();
				int responseCode = con.getResponseCode();
				System.out.println("\nSending 'GET' request to URL : " + arduinoReq);
				System.out.println("Response Code : " + responseCode);
		 
				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
		 
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// 1.5 Release lock
		}
		
		// 1.6 Wait duration
		try {
			sleep(relayReq.duration*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// 1.7 Lock arduinoRelayStatus
		synchronized(lock){
			
			// 1.8 Send OFF
			arduinoRelayStatus[relayNum] = '0';
			String arduinoReq = "http://idanhahn.info.tm:82/" + new String(arduinoRelayStatus);
			try {
				URL arduinoUrl = new URL(arduinoReq);
				HttpURLConnection con = (HttpURLConnection) arduinoUrl.openConnection();
				int responseCode = con.getResponseCode();
				System.out.println("\nSending 'GET' request to URL : " + arduinoReq);
				System.out.println("Response Code : " + responseCode);
		 
				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
		 
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// 1.9 Release lock
		}	
	}
}
