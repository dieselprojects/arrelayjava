package arRelay_java;

public class RelayReqType {
	String startTime; // format hh:mm
	Integer duration; // In seconds
	// onDays format:
	// all - all days in a week
	// none - no days (inactive)
	// [x,y,z] - specific days
	// [x-y] - day x until day y
	String onDays;
	
	RelayReqType(	String 	startTime,
					Integer duration,
					String 	onDays		
			){
		this.startTime	= startTime	;	
		this.duration	= duration	;	
		this.onDays		= onDays	;	
	}
	
	public String print(){
		return "Relay request {Start time, Duration, onDays} {" + startTime + ", " + duration + ", " + onDays + "}";
	}
	
}
